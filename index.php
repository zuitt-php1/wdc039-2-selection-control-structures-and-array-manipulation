<?php require_once'./code.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WDC039-2: Selection Control Structures and Array Manipulation</title>
</head>
<body>
    <h2>Divisible by Five</h2>
    <p><?php printDivisibleOfFive();?></p>

    <h2>Array Manipulation</h2>

    <?php array_push($students, 'John Smith');?>     
    <span>array(<?php echo count($students);?>) {</span>   
    <?php forEach($students as $student){?>
        <span> [<?= studentIndex($student, $students);?>]=> string(<?= strlen($student) ;?>) "<?= $student  ;?>"</span>
    <?php };?>
    <span>}</span>
    <pre><?php echo count($students);?></pre>

    <?php array_push($students, 'Jane Smith');?>     
    <span>array(<?php echo count($students);?>) {</span>   
    <?php forEach($students as $student){?>
        <span> [<?= studentIndex($student, $students);?>]=> string(<?= strlen($student) ;?>) "<?= $student  ;?>"</span>
    <?php };?>
    <span>}</span>
    <pre><?php echo count($students);?></pre>

    <?php array_shift($students);?>     
    <span>array(<?php echo count($students);?>) {</span>   
    <?php forEach($students as $student){?>
        <span> [<?= studentIndex($student, $students);?>]=> string(<?= strlen($student) ;?>) "<?= $student  ;?>"</span>
    <?php };?>
    <span>}</span>
    <pre><?php echo count($students);?></pre>

</body>
</html>